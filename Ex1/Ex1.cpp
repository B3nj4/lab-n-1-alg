/* Escriba un programa que llene un arreglo unidimensional de números enteros y luego obtenga como
resultado la suma del cuadrado de los números ingresados. (Considere un arreglo unidimensional de
tipo entero de N elementos). */

#include <iostream>
using namespace std;

void llenado_vector(int *vec, int n){
    for (int i=0; i<n; i++){
        vec[i] = i;
    }
}

void imprime_vector(int *vec,int n){
    for (int i=0; i<n; i++){
        cout << vec[i] << " ";
    }
    cout << endl;
}

void cuadrado_vector(int *vec,int n){
    for (int i=0; i< n; i++){
        vec[i] = i * i;
    }
}

void sumatoria_array(int *vec,int n){
    int sum;
    for (int i=0; i< n; i++){
        int nuevaVariable = vec[i];
        sum = sum + nuevaVariable;
    }
    cout << "La sumatoria es de: " << sum << "\n\n";
}

int main(int argc, char **argv) {
    int n = 11;
    int vector1[n];

    llenado_vector(vector1,n);
    imprime_vector(vector1,n);
    cuadrado_vector(vector1,n);
    imprime_vector(vector1,n);
    sumatoria_array(vector1,n);
    
    return 0;
}