/* Escriba un programa que lea N frases en un arreglo de caracteres y determine el número de minúsculas
y mayúsculas que hay en cada una de ellas. Puede utilizar las funciones islower() y isupper() de la
librerı́a cctype. */

#include <iostream>
#include <ctype.h>
using namespace std;

void recorre_vector(string palabra){
    for (int i=0; i< palabra.size(); i++){
        palabra[i] = i;
    }
}

void imprime_letra(string palabra){
    for (int i=0; i< palabra.size(); i++){
        cout << palabra[i] << " ";
    }
    cout << endl;
}

void minusculas(string palabra){
    int minuscula = 0;
    for (int i=0; i< palabra.size(); i++){
        char a = palabra[i];
        if(islower(a)){
            //cout << "\nHa entrado " << i << " vez a minuscula\n";
            minuscula++;
        }
    }
    cout << "\nCantidad de minusculas: " << minuscula;
}

void mayusculas(string palabra){
    int mayuscula = 0;
    for (int i=0; i< palabra.size(); i++){
        char a = palabra[i];
        if(isupper(a)){
            //cout << "\nHa entrado " << i << " vez a mayuscula\n";
            mayuscula++;
        }
    }
    cout << "\nCantidad de mayusculas: " << mayuscula;
}

//Main
int main(int argc, char **argv) {
    string palabra;
    int opcion;
    
    do{
        cout << "\nIngrese la palabra: ";
        cin >> palabra;

        recorre_vector(palabra);
        imprime_letra(palabra);
        mayusculas(palabra);
        minusculas(palabra);

        cout << "\n\n¿Desea otra palabra?:";
        cout << "\n 1. Si";
        cout << "\n 2. No";
        cout << "\nOpcion: ";
        cin >> opcion;
    }while(opcion != 2);
    cout << "\n";
    return 0;
}