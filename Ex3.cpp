/* Una empresa registra para cada uno de sus N clientes los siguientes datos:
Nombre (cadena de caracteres)
Teléfono (cadena de caracteres)
Saldo (entero)
Moroso (booleano)
Escriba un programa que permita el ingreso de los datos de los clientes y luego permita imprimir sus
datos indicando su estado (morosos o no). Utilice arreglo de clases en su solución. */

#include <iostream>
using namespace std;
#include "Ex3_clase.h"


int main(int argc, char **argv) {
    
    string nombre,telefono;
    int saldo;
    bool moroso;
    int opcion;
    cout << "Ingrese cantidad de clientes: ";
    cin >> opcion;
    Cliente cliente[opcion];

    for(int i=0; i<opcion; i++){
        cout << "Ingrese nombre del cliente: ";
        cin >> nombre; 
        cout << "Ingrese telefono: ";
        cin >> telefono; 
        cout << "Ingrese saldo: ";
        cin >> saldo; 
        //cout << "Ingrese estado: ";
        //cin >> moroso;  
        cliente[i].set_nombre(nombre);
        cliente[i].set_telefono(telefono);
        cliente[i].set_saldo(saldo);
        cliente[i].set_moroso(true);
    }
    
    for(int j=0; j<opcion; j++){
        if(cliente[j].get_saldo() == 0){
            cliente[j].set_moroso(false);
            cout << "\nCliente no moroso: ";
            cout << "\n-Nombre: " << cliente[j].get_nombre();
            cout << "\n-Saldo: " << cliente[j].get_saldo();
        }
        else if(cliente[j].get_saldo() != 0){
            cout << "\nCliente moroso: ";
            cout << "\n-Nombre: " << cliente[j].get_nombre();
            cout << "\n-Saldo: " << cliente[j].get_saldo();
        }
    }

    return 0;
}