#include <iostream>
using namespace std;

//Importe de archivos necesarios
#include "Ex3_clase.h"

//Variables
Cliente::Cliente() {
    string nombre = "\0";
    string telefono = "\0";
    int saldo;
    bool moroso;
}
    
//Constructor
Cliente::Cliente(string nombre, string telefono, int saldo, bool moroso){
    this->nombre = nombre;
    this->telefono = telefono;
    this->saldo = saldo;
    this->moroso = moroso;
} 

//Metodos
string Cliente::get_nombre(){
    return this->nombre;
}
string Cliente::get_telefono(){
    return this->telefono;
}  
int Cliente::get_saldo(){
    return this->saldo;
}
bool Cliente::get_moroso(){
    return this->moroso;
}


void Cliente::set_nombre(string nombreNuevo) {
    nombre = nombreNuevo;
}
void Cliente::set_telefono(string telefonoNuevo) {
    telefono = telefonoNuevo;
}
void Cliente::set_saldo(int saldoNuevo) {
    saldo = saldoNuevo;
}
void Cliente::set_moroso(bool morosoNuevo) {
    moroso = morosoNuevo;
}



