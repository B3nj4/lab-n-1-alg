#ifndef EX3_CLASE_H
#define EX3_CLASE_H
using namespace std;

//Librerias necesarias
#include <string>

class Cliente {
    private:
        //Variables
        string nombre = "\0";
        string telefono = "\0";
        int saldo;
        bool moroso;

    public:
        //Constructor
        Cliente();
        Cliente(string nombre, string telefono, int saldo, bool moroso);

        //Metodos
        string get_nombre();
        string get_telefono();
        int get_saldo();
        bool get_moroso();

        void set_nombre(string nombreNuevo);
        void set_telefono(string telefonoNuevo);
        void set_saldo(int saldoNuevo);
        void set_moroso(bool morosoNuevo);

};
#endif