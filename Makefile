prefix=/usr/local
CC = g++
CFLAGS = -g -Wall 
SRC = Ex3.cpp Ex3_clase.cpp
OBJ = Ex3.o Ex3_clase.o
APP = Ex3

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 
clean:
	$(RM) $(OBJ) $(APP)
install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin
uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)