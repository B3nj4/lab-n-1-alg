# Lab N°1 - Alg
# Guia Nº1 - Alg

#Ejercicios de Estructura de Datos y Algoritmos

Los ejercicios enumerados del uno al tres, consisten en:
-Ejercicio uno: Programa que llene un arreglo unidimensional de números enteros y que luego obtenga como resultado la suma del cuadrado de los números ingresados. 

-Ejercicio dos: Programa que lea N frases en un arreglo de caracteres y determine el número de minúsculas y mayúsculas que hay en cada una de ellas.

-Ejercicio tres: Programa que tome registro de los clientes de una empresa. (Nombre, Telefono, Saldo, Moroso)

#Pre-requisitos

Para poder instalar el sofware se necesita de un sistema operativo Linux como lo es Ubuntu (link de la instalacion https://ubuntu.com/download)

Es fundamental la instalacion del lenguaje C++ y sus componentes de compilacion en su version g++ 9.3.0 (vease el siguiente link https://hetpro-store.com/TUTORIALES/compilar-cpp-g-linux-en-terminal-leccion-1/)

Se necesitara la instalacion de un editor de texto/IDE como lo puede ser "Visual Studio Code". (vease el link para la instalacion https://ubunlog.com/visual-studio-code-editor-codigo-abierto-ubuntu-20-04/?utm_source=feedburner&utm_medium=%24%7Bfeed%2C+email%7D&utm_campaign=Feed%3A+%24%7BUbunlog%7D+%28%24%7BUbunlog%7D%29)

Por ultimo, para poder clonar el repositorio del sofware, es necesario tener instalado "Gitlab" correctamente.


#Comenzando  

Para poder acceder a este sofware se requerira de clonar el espacio de trabajo desde https://gitlab.com/B3nj4/lab-n-1-alg.git usando la siguiente combinacion en su terminal Linux

    git clone https://gitlab.com/B3nj4/lab-n-1-alg.git

Luego, se debera ejecutar el archivo "Makefile" con la finalidad de poder compilar nuestro programa. Para ello debera ingresar en su terminal Linux el siguiente comando

    make

Ya ejecutado el "Makefile" debera ejecutar el programa respectivo "Ex1.cpp", "Ex3.cpp", "Ex3.cpp" dependiendo de la carpeta en la que se posicione y asi poder interactuar con el sofware, para ello es fundamental ingresar lo siguiente en su terminal Linux

    ./Ex1 

    ./Ex2

    ./Ex3

De esta forma el usuario podra interactuar con los diferentes sofware.


#Construido con

Para la creacion de este proyecto se necesito del editor de texto Visual Studio Code y el lenguaje de programacion "C++" 


#Autor
    
    Benjamin Vera Garrido - Ingenieria Civil en Bioinformatica


